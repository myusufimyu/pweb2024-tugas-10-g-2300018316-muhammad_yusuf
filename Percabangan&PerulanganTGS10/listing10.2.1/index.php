<?php
$nilai_angka = [70, 86];

function KonversikanNilai($nilai){
    if ($nilai >= 85){
        return 'A';
    } elseif ($nilai >= 70) {
        return 'B';
    } elseif ($nilai >= 60) {
        return 'C';
    } elseif ($nilai >=50) {
        return 'D';
    } else {
        return 'E';
    }
}

foreach ($nilai_angka as $nilai) {
    $nilai_huruf = KonversikanNilai($nilai);
    echo "Nilai Anda $nilai, konversi nilai huruf : $nilai_huruf <br>";
}
?>